My answers to Assignment 5

1. Logger.log allows you to store a message into a retrievable file, which can give important information regarding tests.

2. It is found in the logger.log of the file logger.properties.

3. AssertThrows verifies that function throws an exception of the expected type and returns the exception.

4.
	
	1. During serialization, Java runtime creates a version number for a class, so that it can de-serialize it later. This is the SerialVersionUID. The SerialVerisionUID verifies that deserialization does not throw an exception.

	2. We need to override the constructor because otherwise the compiler might otherwise construct the parent class, Exception, rather than the timerException.

	3. The TimerException will only allow the two constructors specified. If another constructer with different parameter types are called, Exception's constructor would otherwise be called.

5. A Static block is used for initializing the static variables.This block gets executed when the class is loaded in the memory, and is only initialized once. 

6. Bitbucket's Server displays the Readme.md's contents on the repository's Overview page if the file has the .md extension.

7. The test is failing because the timeNow variable is initialized to null, and the following try block throws an exception because the time is not greater than zero. 
   The finally block catches the throw, so the time variable is never initialized. We can fix this by setting the time to not null right away.

8. The actual issue is that the program throws a NullPointerException when a TimerException is expected. As a result, the finally block executes rather than the method function. When the   method function does eventually get called, a NullPointerException is thrown. Because the test failOverTest() requires a TimerException, the entire test fails.

9. N/A

10. N/A

11. NullPointerException is a Runtime Exception and TimerException is a normal exception.

12. N/A